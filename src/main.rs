#![feature(plugin)]
#![plugin(rocket_codegen)]

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_codegen;
extern crate postgres;
extern crate r2d2;
extern crate r2d2_diesel;
extern crate r2d2_postgres;
extern crate rocket;
extern crate rocket_contrib;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

mod flags;

use std::env;

use rocket::config::{Config, Environment};

fn main() {
    let config = Config::build(Environment::Production)
        .address("0.0.0.0")
        .port(8080)
        .finalize()
        .expect("Could not configure rocket");
    let app = rocket::custom(config, false);
    let database_url = env::var("DATABASE_URL").expect("Could not get DB URL.");
    let pool = flags::db::init_pool(&database_url);
    app.manage(pool)
        .mount("/flags",
               routes![flags::views::index,
                       flags::views::get,
                       flags::views::post,
                       flags::views::delete])
        .launch();
}
