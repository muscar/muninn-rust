use std::ops::Deref;

use diesel::pg::PgConnection;

use r2d2;
use r2d2_diesel::ConnectionManager;

use rocket::request::{self, FromRequest};
use rocket::Outcome;
use rocket::{Request, State};
use rocket::http::Status;

pub type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;

pub struct Conn(r2d2::PooledConnection<ConnectionManager<PgConnection>>);

impl Deref for Conn {
    type Target = PgConnection;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for Conn {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Conn, ()> {
        let pool = match <State<Pool> as FromRequest>::from_request(request) {
            Outcome::Success(pool) => pool,
            Outcome::Failure(e) => return Outcome::Failure(e),
            Outcome::Forward(_) => return Outcome::Forward(()),
        };

        match pool.get() {
            Ok(conn) => Outcome::Success(Conn(conn)),
            Err(_) => Outcome::Failure((Status::InternalServerError, ())),
        }
    }
}

pub fn init_pool(url: &str) -> Pool {
    let config = r2d2::Config::builder().pool_size(16).build();
    let manager = ConnectionManager::<PgConnection>::new(url);
    r2d2::Pool::new(config, manager).expect("Failed to create pool.")
}
