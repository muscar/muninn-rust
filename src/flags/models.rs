use super::schema::flags;

#[derive(Insertable)]
#[derive(Queryable)]
#[table_name="flags"]
#[derive(Clone, Serialize, Deserialize)]
pub struct Flag {
    pub name: String,
    pub description: Option<String>,
    pub enabled: bool,
}
