use diesel;
use diesel::*;
use diesel::pg::PgConnection;
use diesel::result::QueryResult;

use flags::models::Flag;
use flags::schema::flags::dsl::*;

pub fn all(conn: &PgConnection) -> QueryResult<Vec<Flag>> {
    flags.load(conn)
}

pub fn get(conn: &PgConnection, flag_name: &str) -> QueryResult<Flag> {
    flags.filter(name.eq(flag_name)).get_result(conn)
}

pub fn put(conn: &PgConnection, flag: &Flag) {
    diesel::insert(flag)
        .into(flags)
        .execute(conn)
        .expect("Could not save flag");
}

pub fn delete(conn: &PgConnection, flag_name: &str) {
    diesel::delete(flags.filter(name.eq(flag_name)))
        .execute(conn)
        .expect("Could not delete flag.");
}
