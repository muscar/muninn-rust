use diesel::result::QueryResult;
use rocket_contrib::JSON;

use flags::db::Conn;
use flags::models::Flag;

#[get("/")]
pub fn index(conn: Conn) -> QueryResult<JSON<Vec<Flag>>> {
    super::service::all(&conn).map(JSON)
}

#[get("/<name>")]
pub fn get(name: &str, conn: Conn) -> QueryResult<JSON<Flag>> {
    super::service::get(&conn, name).map(JSON)
}

#[post("/", format = "application/json", data = "<flag>")]
pub fn post(flag: JSON<Flag>, conn: Conn) {
    super::service::put(&conn, &flag.into_inner());
}

#[delete("/<name>")]
pub fn delete(name: &str, conn: Conn) {
    super::service::delete(&conn, name);
}
