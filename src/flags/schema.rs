table! {
    flags (name) {
        name -> VarChar,
        description -> Nullable<Text>,
        enabled -> Bool,
    }
}
