builder-image:
	docker build -t rust-debian-builder -f Dockerfile.builder .

production-image:
	docker build -t muninn-rust .

build:
	docker run --rm -it -v "$(shell pwd)":/home/rust/src rust-debian-builder cargo build --release

run-local:
	docker run --rm --name muninn-rust -e "DATABASE_URL=$(DATABASE_URL)" --add-host=database:10.200.10.1 -p 8080:8080 -it muninn-rust
