CREATE TABLE flags (
    name VARCHAR(512) NOT NULL,
    description TEXT,
    enabled BOOLEAN NOT NULL,
    PRIMARY KEY(name)
)
