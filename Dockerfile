FROM ubuntu:zesty

RUN apt-get update && \
    apt-get install -y \
        libpq-dev

ADD target/release/muninn /  

EXPOSE 8080

CMD ["/muninn"]
