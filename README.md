# Muninn

Muninn is a feature flag service. At the moment if exposes a simple CRUD REST
API.

# Prerequisites

A local instance of PostgresSQL.

# Setup

Clone the repo.

```
$ git clone git@gitlab.com:muscar/muninn-rust.git

$ cd muninn-rust
```

Expose the local host to the docker container so the app can access the DB.

```
$ sudo ifconfig lo0 alias 10.200.10.1/24
```

Finally, build the docker image used to compile the app.

```
$ make builder-image
```

# Building

Once we have a builder image, buildign the app is simple. Just run the `make`
command and go grab a coffee, it's gonna take a while.

```
$ make build production-image
```

# Running locally

Just run the foillowing `make` command, passing the DB URL as an environment var:

```
$ env DATABASE_URL="..." make run-local
```

*NOTE:* the Makefile maps the host `database` to `10.200.10.1` so your DB URL
should look something like `"postgres://foo@database:5432"`

# Test that it works

Assumign you're using `httpie`:

```
$ http POST http://0.0.0.0:8080/flags/ name=first_flag description="A test flag." enabled=true --json

$ http GET http://0.0.0.0:8080/flags/
```
